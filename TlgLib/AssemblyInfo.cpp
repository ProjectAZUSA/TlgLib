#include "stdafx.h"

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

[assembly:AssemblyTitleAttribute(L"TlgLib")];
[assembly:AssemblyDescriptionAttribute(L"")];
[assembly:AssemblyConfigurationAttribute(L"")];
[assembly:AssemblyCompanyAttribute(L"Project AZUSA")];
[assembly:AssemblyProductAttribute(L"FreeMote")];
[assembly:AssemblyCopyrightAttribute(L"Copyrigt(c) Project AZUSA 2018")];
[assembly:AssemblyTrademarkAttribute(L"wdwxy12345@gmail.com")];
[assembly:AssemblyCultureAttribute(L"")];
[assembly:ExtensionAttribute]

[assembly:AssemblyVersionAttribute("1.0.*")];

[assembly:ComVisible(false)];

[assembly:CLSCompliantAttribute(true)];
